# Web Boilerplate

Web Boilerplate is a front-end starter kit for building fast, robust, and adaptable web applications or sites.

## Use

Install necessary node modules

        npm install

Build out the directory:

        gulp build

Start the dev server by running one of the two following commands in terminal:

        gulp
        gulp serve

## Directory Structure

All development files reside in the /src directory.
All build files are compiled to the /dist directory.

        src/
            - css/
            - fonts/
            - images/
            - js/
            - scss/
                - components/
                    _header.scss
                    _footer.scss
                    ...
                - core/
                    _base.scss
                    _fonts.scss
                    _variables.scss
                    ...
                - elements/
                    _buttons.scss
                    _carousel.scss
                    ...
                - lib/
                    - mixins/
                    _normalize.scss
                    ...
                main.scss
            - templates/
